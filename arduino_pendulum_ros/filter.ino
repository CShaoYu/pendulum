struct ANGLE filter() {
  sensor->acce_z = sensor->acce_z + ACC_Z_FIX;
  sensor->acce_y = sensor->acce_y + ACC_Y_FIX;
  radian = atan2(sensor->acce_y, sensor->acce_z);
  angle->angle_acc = (180.0 * radian) / PI; //unit:deg/s
//因板子上電路環境並不是能麼理想，因此一開始的0.005*120=0.6秒內捕捉acc初始值，之後的acc以此為offset﹑
//使用條件；每次系統開始工作前都以校正的初始狀態出發 也就是我們校正的0度位置
  accFlag += 5;
  if (accFlag < 120)
  {
    if (angle->angle_acc > 0.0)  
      acc_offset = angle->angle_acc;  
  }
  angle->angle_acc = angle->angle_acc - acc_offset;


  sensor->gyro_x = -(sensor->gyro_x - sensor->gyro_verf); //使其與加速度計之軸同向
  sensor->gyro_x = sensor->gyro_x - GYRO_X_FIX;
  //tilt_angular_velocity = (double)adc_buff[GYRO_X]*1.7;//mRadian per second  
  //tmd = (double)sensor->gyro_x*1.7; //mRadian per second
  //tmd = tmd*0.18/PI; //mRadian per second  ->  Radian per second -> deg per second,  unit:deg/s  
  angular_velocity  = (double)sensor->gyro_x*1.7; //mRadian per second
  angular_velocity = angular_velocity*0.18/PI; //mRadian per second  ->  Radian per second -> deg per second,  unit:deg/s    

  angular_velocity_rc = 0.9*angular_velocity_old + 0.1*angular_velocity;  
  angular_velocity_old = angular_velocity_rc; 
  
//  //angle->angle_gyro = angle->angle_gyro_old + sensor->gyro_x  * sample;
//  angle->angle_gyro = angle->angle_gyro_old + angular_velocity_rc *sample; 
//  //angle->angle_gyro = angle->angle_gyro_old + ((double)sensor->gyro_x*1.7*sample*0.18)/PI;
//  angle->angle_gyro_old = angle->angle_gyro;
//
  if (abs(angular_velocity_rc) < 0.25)
    angle->angle_gyro = angle->angle_gyro;
  else
    angle->angle_gyro = angle->angle_gyro_old + angular_velocity_rc *sample;    
  angle->angle_gyro_old = angle->angle_gyro;

  double num = time_filter / sample;
  double den = 1 + (time_filter / sample);
  double a = num / den; //double a = (time_filter/sample) / (1+(time_filter/sample)); //不知為何不行
  //angle->angle_filter = 0.7*angle_filter_old + 0.3*(a * (angle->angle_filter_old + sensor->gyro_x  * sample) + (1 - a) * (angle->angle_acc));
  // 高低
//  angle->angle_filter = a * (angle->angle_gyro_old + angular_velocity  * sample) + (1 - a) * (angle->angle_acc);
//  angle->angle_filter = a * (angle->angle_gyro_old + angular_velocity_rc * sample) + (1 - a) * (angle->angle_acc);
  angle->angle_filter = a * (angle->angle_gyro) + (1 - a) * (angle->angle_acc);
  // 高低串高低  
//  angle->angle_filter = 0.7*angle->angle_filter_old + 0.3*(a * (angle->angle_gyro_old + angular_velocity  * sample) + (1 - a) * (angle->angle_acc));
//  angle->angle_filter_old = angle->angle_filter;   
  return *angle;
}
