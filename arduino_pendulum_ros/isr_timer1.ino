ISR(TIMER1_OVF_vect) {
  TCNT1 = (65535 - 1249); ////重新裝載  5ms
  cnt += 1;   //第一次執行=1
  cnt3 += 5;
  cnt_time += 5;

  filter();

  if (flag_pid_balance == 1) {
    if (cnt3 >= 150) {  //換BLD 300B驅動後 啟動TIMER時加速度計會有很大的值  故等一會兒熱機一下 可能是電路有啥干擾八, 150ms    

      odm_L = dis_L_tmp * 1.2227; // 24.13/62.0 *PI
      odm_R = dis_R_tmp * 1.2227;
      //hall訊號接收時受到干擾, 故以此做濾波, 正常順轉部會有事, 會有事的條件為:平衡模式下故意讓她高頻來回控制
      if (abs(odm_L - odm_L_old) > 7.2)  //輪胎1pulse約轉動5.625度, 6pulse約33.75度, 6pulse*1.2227約 7.2
        odm_L = odm_L_old;
      odm_L_old = odm_L;             
      if (abs(odm_R - odm_R_old) > 7.2) 
        odm_R = odm_R_old;
      odm_R_old = odm_R;        
      if ((odm_L >= refOdm) || (odm_R >= refOdm))  
        locating = 1;               

      if  ( vel_L_tmp >= 580) //max 40 rpm
        vel_L = 23438.00 / vel_L_tmp;
      else   //max 40 rpm 根據空載的紀錄pwm40時轉速約為30rpm, 但現在落地一定更低, 而我max又是50而以所以綽綽有餘
        vel_L = vel_L; //vel_L = 0;                
             
      if  ( vel_R_tmp >= 580) //max 40 rpm
        vel_R = 23438.00 / vel_R_tmp;
      else
        vel_R = vel_R; //vel_R = 0;        

      acc_L = (vel_L - vel_old_L) * 5.0; //  /0.2
      acc_R = (vel_R - vel_old_R) * 5.0; //  /0.2

      avg_odm = (odm_L + odm_R) * 0.5; //  /2.0;
      avg_vel = (vel_L + vel_R) * 0.5; //  /2.0;
      avg_acc = (acc_L + acc_R) * 0.5; //  /2.0;   

      vel_old_L = vel_L;
      vel_old_R = vel_R;        

      FSMC_calculate(&refAngle, &(angle->angle_filter), &pleft_balance);
      if (pleft_balance.controlVal < 0)
      {
        forward();
        OCR3A = abs((int)pleft_balance.controlVal);
        TCCR3A |= _BV(COM3A1);
        OCR4A = abs((int)pleft_balance.controlVal);
        TCCR4A |= _BV(COM4A1);
      }
      else
      {
        backward();
        OCR3A = abs((int)pleft_balance.controlVal);
        TCCR3A |= _BV(COM3A1);
        OCR4A = abs((int)pleft_balance.controlVal);
        TCCR4A |= _BV(COM4A1);
      }                 
    }    
  }

  
  flag_acgy = 1;
  if (cnt == 1) {
    flag_print = 1;
    cnt = 0;
  }
}

ISR(TIMER2_OVF_vect) {
  TCNT2 = (255 - 79);  //230, 9375, -199, 100us

//  if (pulse_L >= 4294967295) {  //40us * 4,294,967,295 = 171,798.6918s, 若輪子沒轉動最多可等到該秒數才重製
//  if (pulse_L >= 23438) { //40us * 23438 = 0.93752s,  0.015625rev/0.93752 = 0.01667rev/s = 1rpm
/*******
 * 空轉最低轉速4.8rpm = 4.8/60 = 0.08rev/s = 0.08rps
 * 單一encoder間隔 : 1/64 約等於 0.015625rev
 * 單一encoder脈波時間 = 位置/速度 = 0.015625/0.08 = 0.1953125秒
 * 所以理論上pulse_L應該為若大於約 0.1953125/0.00004 = 4882.8125個Pulse就要歸零，意思即為在最低轉速下每0.1953125秒可更新(解析)一次編碼器值
 * 推測40us與實際時間應該還是會有些許誤差(實時性問題), 故小數點部分取4882, 0.1953125/4882 = 40us多一點
 * 所以理論上判斷式應該要是下式
 * if (pulse_L >= 4882) 
 * 套用此式意思即為，馬達在最低轉速下每0.2秒會更新一次值(外部中斷同時reset pulse)，超過0.2秒就視為馬達停止轉動
 * 但實際測試發現套用此式時在segway慢慢往前時會有過度歸零的情況，原因可能為因我是直接使用平衡控制並藉由手推往前的方式
 * 可能原本應該要觸發外部中斷重新計算一次pulse了，但因平衡控制的關係所以segway傾角被救回來且控制量下降所以在觸發外部中斷前就停了下來(畢竟1個pulse間相差360/64. 5.625度左右)，
 * 導致沒有觸發到，不管速度多少此情況都有可能發生，尤其在相對低速下此情況更明顯，因此T & E發現設為15000(0.6s)可相較有效避開此情況，也不會太慢判斷速度為0
*******/
  if (pulse_L >= 15000) { //40us * 15000 = 0.6s    
    pulse_L = 0;
    switch_count_pulse_L = 0;
    vel_L_tmp = 0;
    vel_L = 0;        
  }
  else pulse_L += 1;

  if (pulse_R >= 15000) {
    pulse_R = 0;
    switch_count_pulse_R = 0;
    vel_R_tmp = 0;
    vel_R = 0;    
  }
  else pulse_R += 1;
}

//ISR(TIMER5_OVF_vect) {
//
//  TCNT5 = (65535 - 199);
//  pulse_R += 1;
//}
