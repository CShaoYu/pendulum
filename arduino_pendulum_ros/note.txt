刪掉以下部分

else if (flag_display_type == 1) {
  flag_sine
  flag_constant
  flag_pid_velocity
  flag_pid_velocity_balance
}


ROS 部分只針對msg_pendulum_status做新增，
其餘想透過ROS pub改參數(arduiino sub)的都拔除，改參數直接透過重新燒入比較快
ros arduino也不用一直輪循訂閱

因此以下的也刪掉了
void set_ref_pwmVal_and_RPM_int( custom_msgs::pwmVal_refRPM &msg) {}
void show_param_all( std_msgs::String &msg) {}
void set_param_left_fsmc_balance( custom_msgs::left_fsmc_balance &msg) {}
void set_param_left_pid_balance( custom_msgs::left_pid_balance &msg) {}
void set_param_left_fsmc_velocity( custom_msgs::left_fsmc_velocity &msg) {}
void set_param_left_pid_velocity( custom_msgs::left_pid_velocity &msg) {}
void set_param_right_fsmc_balance( custom_msgs::right_fsmc_balance &msg) {}
void set_param_right_pid_balance( custom_msgs::right_pid_balance &msg) {}
void set_param_right_fsmc_velocity( custom_msgs::right_fsmc_velocity &msg) {}
void set_param_right_pid_velocity( custom_msgs::right_pid_velocity &msg) {}
void set_ref_Odm_float( custom_msgs::pendulum_status &msg) {}