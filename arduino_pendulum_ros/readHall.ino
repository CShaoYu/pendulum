void hall_L() {
  switch_count_pulse_L += 1;
  if (switch_count_pulse_L == 1) {
    vel_L_tmp_start = pulse_L ;
  }
  else if (switch_count_pulse_L == 2) {
    vel_L_tmp  = pulse_L - vel_L_tmp_start;      
    pulse_L = 0;
    switch_count_pulse_L = 0;
  }
/* // 當時的一秒一次的速度控制, 需持續轉動
  //最高rpm不會超過255, BLeD 300B這個驅動會超過 不過我也不會到這麼快的轉速
  __asm__ __volatile__
  (
    "LDS R2, (rpm_L_tmp) \n"
    "INC R2 \n"
    "STS (rpm_L_tmp), r2"
  );
 */
  if (flag_direction == Direction::Forward) { //我不會組合語言的取進位, 超過255也就是256的時候就重回1開始
    dis_L_tmp += 1;     //62一圈約24.13*3.14159公分
  }
  else if (flag_direction == Direction::Backward) {  //DEC的話會從256開始扣 20191028我猜估計是要先補數再加負號(NEG)然後再add原本的數字
    dis_L_tmp -= 1;
  }
  //PORTG ^= (1 << 1); // 1=0^1, 0=1^1, 1=0^1... xor  //digital pin13, 接上示波器檢查是否每次的print都還在0.001s取樣時間內
}

void hall_R() {
  switch_count_pulse_R += 1;
  if (switch_count_pulse_R == 1) {
    vel_R_tmp_start = pulse_R ;
  }
  else if (switch_count_pulse_R == 2) {
    vel_R_tmp  = pulse_R - vel_R_tmp_start;
    pulse_R = 0;
    switch_count_pulse_R = 0;
  }
/* // 當時的一秒一次的速度控制, 需持續轉動  
  __asm__ __volatile__
  (
    "LDS R2, (rpm_R_tmp) \n"
    "INC R2 \n"
    "STS (rpm_R_tmp), r2"
  );
*/
  if (flag_direction == Direction::Forward) {
    dis_R_tmp += 1;
  }
  else if (flag_direction == Direction::Backward) {  //DEC的話會從256開始扣 20191028我猜估計是要先補數再加負號(NEG)然後再add原本的數字, 簡單來說就是我不會負號的處理再組合語言上s
    dis_R_tmp -= 1;
  }
}
