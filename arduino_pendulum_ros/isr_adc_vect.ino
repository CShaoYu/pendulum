ISR(ADC_vect) {

  if (flag_acgy) {
    adc_channel = ADMUX & (_BV(MUX3) | _BV(MUX2) | _BV(MUX1) | _BV(MUX0) ) ;

    if (adc_channel == 0x00) {     //我花了兩天還是不知道為甚麼順序會跑掉 所以採用值後再指定buffer位置
      if (sw_acgy) *(buff_acgy + adc_adress - 1) = (ADCL | (ADCH << 8));
      else *(buff_acgy + adc_adress) = (ADCL | (ADCH << 8));
    }
    else if (adc_channel == 0x01) {
      if (sw_acgy) *(buff_acgy + adc_adress - 1) = (ADCL | (ADCH << 8));
      else *(buff_acgy + adc_adress) = (ADCL | (ADCH << 8));
    }
    else if (adc_channel == 0x02) {
      if (sw_acgy) *(buff_acgy + adc_adress - 1) = (ADCL | (ADCH << 8));
      else *(buff_acgy + adc_adress) = (ADCL | (ADCH << 8));
    }
    else if (adc_channel == 0x03) {
      handle_bar = (ADCL | (ADCH << 8));
    }
    adc_adress += 1;
    ADMUX += 1;
    if (adc_adress == 4) {
      ADCSRB |= _BV(MUX5);
      ADMUX = 0b01000000;
      sw_acgy = 1;
    }
    else if (adc_adress == 7) {
      adc_adress = 0;
      ADCSRB &=  ~_BV(MUX5);
      ADMUX = 0b01000000;
      sw_acgy = 0;
    }
    sensor->acce_y = buff_acgy[0];
    sensor->acce_x = buff_acgy[1];
    sensor->acce_z = buff_acgy[2];
    sensor->gyro_x  = buff_acgy[3];
    sensor->gyro_verf  = buff_acgy[4];
    sensor->gyro_z  = buff_acgy[5];
    flag_acgy = 0;
  }
  //  // set ADSC in ADCSRA, ADC Start Conversion
  //  // next adc interrupt will be in about 1664 mcu clocks
  ADCSRA |= (1 << ADSC);
}
