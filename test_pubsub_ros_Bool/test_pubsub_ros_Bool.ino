/*
   rosserial PubSub Example
   Prints "hello world!" and toggles led
*/

#include <ros.h>
#include <std_msgs/String.h>
#include <std_msgs/Bool.h>

bool hello = 0 ;


ros::NodeHandle nh;

std_msgs::String str_msg;
ros::Publisher chatter("str_msggg", &str_msg);


void messageCb( const std_msgs::Bool toggle_msg) {

  hello = toggle_msg.data;
  if (hello) {
    while (Serial.available() == 0) {}
    str_msg.data = "fuck";
    hello = 0;
  }
  else {
    str_msg.data = "pwm set" ;
  }

  chatter.publish( &str_msg );

}
ros::Subscriber<std_msgs::Bool> sub("command", messageCb );


void setup()
{
  nh.initNode();
  nh.advertise(chatter);
  nh.subscribe(sub);
}

void loop()
{
  nh.spinOnce();

  delay(1000);
}
