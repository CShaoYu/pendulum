#define ACC_Y_FIX - 367 //361 341 295
#define ACC_X_FIX -349
#define ACC_Z_FIX -295  //理想值扣完約163.84
#define GYRO_X_FIX 3
#define GYRO_Z_FIX 4

volatile int adc_channel = 0;

//int ACCE_Y = A0;
//int ACCE_X = A1;
//int ACCE_Z = A2;
//int GYRO_X = A8;// 8  ,5
//int GYRO_Verf = A9;  //   9 ,3
//int GYRO_Z = A10;  //10  ,4


volatile int sensorACCE_Y = 0;
volatile int sensorACCE_X = 0;
volatile int sensorACCE_Z = 0;
volatile int sensorGYRO_X = 0;
volatile int sensorGYRO_Verf = 0;
volatile int sensorGYRO_Z = 0;

int filter = 1 ;

unsigned long time_old = 0;
int angle_filter_old = 0;
double time_filter = 0.11; // %
double sample = 0.01;  //unit: second

double radian = 0;
double angle = 0;
double angle_filter = 0;
double angle_gyro = 0;
double angle_gyro_old = 0;

bool show = 0;
byte abc ;
bool swAcGy = 1; //switch read acc gyro

volatile int* acGybuff = new volatile int[6];

int handle_bar = 0;

void setup() {
  Serial.begin(2000000);
  DDRG |= (1 << 1); //digital pin40, PG1
  initAnalog();
}
void loop() {
  char temp;

  time_old = millis();

  if (Serial.available() > 0) {
    temp = Serial.read();
    if (temp == 'q') {
      temp = Serial.read(); //clear Serial buffer
      Serial.println("hi bitch");
      while (Serial.available() == 0) {}
      filter = Serial.parseInt();
      Serial.print("pwmLval: "); Serial.println(filter);
      temp = Serial.read(); //clear Serial buffer
    }
  }
  if (filter == 0) {
    if (show) {

      //Serial.print(ADCSRB, BIN);
      //Serial.print(",,, ");
      //Serial.print(abc, BIN);
      //Serial.print(",,, ");
      //      Serial.print(ADMUX, BIN);  //直接print MUX好像是其他東西
      //      Serial.print(", ");
      //      Serial.print(adc_channel);
      //      Serial.print(", ");
      //我花了兩天還是不知道為甚麼順序會跑掉 所以採用值後再指定buffer位置
      Serial.print(handle_bar); Serial.print(", ");
      Serial.print(acGybuff[0]); Serial.print(", "); Serial.print(acGybuff[1]); Serial.print(", ");
      Serial.print(acGybuff[2]); Serial.print(", "); Serial.print(acGybuff[3]); Serial.print(", ");
      Serial.print(acGybuff[4]); Serial.print(", "); Serial.println(acGybuff[5]);
      //      Serial.print(sensorACCE_X); Serial.print(", "); Serial.print(sensorACCE_Y); Serial.print(", ");
      //      Serial.print(sensorACCE_Z);//XYZ
      //      Serial.print(", ");
      //      Serial.print(sensorGYRO_X); Serial.print(", ");
      //      Serial.print(sensorGYRO_Verf); Serial.print(", ");
      //      Serial.println(sensorGYRO_Z);

      //Serial.print(sensorACCE_X + ACC_X_FIX); Serial.print(", "); Serial.print(sensorACCE_Y + ACC_Y_FIX); Serial.print(", ");
      //Serial.print(sensorACCE_Z + ACC_Z_FIX); Serial.print(", "); Serial.println(bit2volt(sensorACCE_Z + ACC_Z_FIX, 1.0)); // the same with: Serial.println(((sensorACCE_Z+ACC_Z_FIX)*5.0)/1024.0);//放平修正

      //          sensorACCE_Z = sensorACCE_Z + ACC_Z_FIX;
      //          sensorACCE_Y = sensorACCE_Y + ACC_Y_FIX;
      //          radian = atan2(sensorACCE_Y, sensorACCE_Z);
      //          angle = (180.0 * radian) / PI;
      //          Serial.println(angle);
      //
      //          Serial.println(sensorGYRO_X-sensorGYRO_Verf);
      //          Serial.println(sensorGYRO_Verf-sensorGYRO_Verf);
      //          Serial.println(sensorGYRO_Z-sensorGYRO_Verf);
      //          Serial.print(sensorGYRO_X - sensorGYRO_Verf); Serial.print(", "); Serial.print(sensorGYRO_Verf - sensorGYRO_Verf); Serial.print(", "); Serial.println(sensorGYRO_Z - sensorGYRO_Verf);



      //          sensorACCE_Z = sensorACCE_Z + ACC_Z_FIX;
      //          sensorACCE_Y = sensorACCE_Y + ACC_Y_FIX;
      //          radian = atan2(sensorACCE_Y, sensorACCE_Z);
      //          angle = (180.0 * radian) / PI;
      //          Serial.print(angle); Serial.print(", ");
      //
      //          sensorGYRO_X = sensorGYRO_X - sensorGYRO_Verf;
      //          sensorGYRO_Z = sensorGYRO_Z - sensorGYRO_Verf;
      //          //    Serial.print(sensorGYRO_X+GYRO_X_FIX);Serial.print(", ");Serial.println(sensorGYRO_Z+GYRO_Z_FIX);
      //          angle_gyro = angle_gyro_old + sensorGYRO_X * ((millis() - time_old) / 1000.0);
      //          Serial.println(angle_gyro);
      //          angle_gyro_old = angle_gyro ;
      show = 0;
    }
  }
  else if (filter == 1) {
    if (show) {
      sensorACCE_Z = sensorACCE_Z + ACC_Z_FIX;
      sensorACCE_Y = sensorACCE_Y + ACC_Y_FIX;
      radian = atan2(sensorACCE_Y, sensorACCE_Z);
      angle = (180.0 * radian) / PI;

      sensorGYRO_X = -(sensorGYRO_X - sensorGYRO_Verf); //使其同向
      angle_gyro = angle_gyro_old + sensorGYRO_X * ((millis() - time_old) / 1000.0);
      angle_gyro_old = angle_gyro;

      //int angle = 0.9*(angle_old + sensorGYRO_X*(millis()-time_old)) + 0.1*(1000*sensorACCE_Y/sensorACCE_Z);
      double num = time_filter / sample;
      double den = 1 + (time_filter / sample);
      double a = num / den; //double a = (time_filter/sample) / (1+(time_filter/sample)); //不知為何不行
      angle_filter = a * (angle_filter_old + sensorGYRO_X * sample) + (1 - a) * (angle);
      Serial.print(millis() / 1000.0); Serial.print(", ");
      Serial.print(angle); Serial.print(", "); Serial.print(angle_gyro); Serial.print(", "); Serial.println(angle_filter);
      angle_filter_old = angle_filter;
      if ((millis() / 1000.0) > 10) {
        //delay(20000);
      }
      show = 0;
    }
  }
}

template <class T1, class T2>
inline T2 bit2volt(T1 a, T2 b) { //T2:1, return intergral; T2:1.0, return float;
  return (b * (a * 5) / (b * 1024));
}

ISR(ADC_vect) {
  abc = ADMUX & (_BV(MUX3) | _BV(MUX2) | _BV(MUX1) | _BV(MUX0) ) ;
  if (!show) {
    if (abc == 0x00) {     //我花了兩天還是不知道為甚麼順序會跑掉 所以採用值後再指定buffer位置
      if (swAcGy) *(acGybuff + adc_channel - 1) = (ADCL | (ADCH << 8));
      else *(acGybuff + adc_channel) = (ADCL | (ADCH << 8));
    }
    else if (abc == 0x01) {
      if (swAcGy) *(acGybuff + adc_channel - 1) = (ADCL | (ADCH << 8));
      else *(acGybuff + adc_channel) = (ADCL | (ADCH << 8));
    }
    else if (abc == 0x02) {
      if (swAcGy) *(acGybuff + adc_channel - 1) = (ADCL | (ADCH << 8));
      else *(acGybuff + adc_channel) = (ADCL | (ADCH << 8));
    }
    else if (abc == 0x03) {
      handle_bar = (ADCL | (ADCH << 8));
    }
    adc_channel += 1;
    ADMUX += 1;
    if (adc_channel == 4) {
      ADCSRB |= _BV(MUX5);
      ADMUX = 0b01000000;
      swAcGy = 1;
    }
    else if (adc_channel == 7) {
      adc_channel = 0;
      ADCSRB &=  ~_BV(MUX5);
      ADMUX = 0b01000000;
      swAcGy = 0;
    }
    sensorACCE_Y = acGybuff[0];
    sensorACCE_X = acGybuff[1];
    sensorACCE_Z = acGybuff[2];
    sensorGYRO_X = acGybuff[3];
    sensorGYRO_Verf = acGybuff[4];
    sensorGYRO_Z = acGybuff[5];

    show = 1;
  }
  //  // set ADSC in ADCSRA, ADC Start Conversion
  //  // next adc interrupt will be in about 1664 mcu clocks
  ADCSRA |= (1 << ADSC);
  PORTG ^= (1 << 1);
}

void initAnalog(void) {
  adc_channel = 0;

  cli();
  //ADMUX |= (1 << ADLAR); //左移10bit內的左邊8個bit將其放在ADCH（不太清楚用意為何），選用
  ADMUX = 0b01000000;
  //設定ADC read 預除器
  //ADCSRA |= (1<<ADPS2);
  //ADCSRA |= (1<<ADPS1);
  //ADCSRA |= (1<<ADPS0);
  //ADCSRA |= (1 << ADATE); //enable auto trigger 需再往下看ADCSRB內的ADTS2：0
  //ADCSRA |= (1 << ADIE); //enable interrupts when measurement complete
  //ADCSRA |= (1 << ADEN); //enable ADC
  ADCSRA = _BV(ADATE) | _BV(ADIE) | _BV(ADEN) | _BV(ADPS2) | _BV(ADPS1) | _BV(ADPS0); //111:128(示波器實際勾約為550Hz*2) ,110:64(示波器實際勾約為930Hz*2), 101:32(示波器實際勾約為1.6kHz*2), *2是因為我是xorLED做亮暗切換, 但亮跟暗都算是觸發一次此adc_vect
  ADCSRB = 0; //free Running mode
  ADCSRA |= (1 << ADSC); //start ADC measurement,set ADSC in ADCSRA, ADC Start Conversion
  //ADCSRB = (1<< MUX5);

  sei();// Enable global interrupts
}
