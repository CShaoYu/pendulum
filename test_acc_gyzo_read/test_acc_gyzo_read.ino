#define ACC_Y_FIX -367  //361 341 295
#define ACC_X_FIX -349
#define ACC_Z_FIX -295  //理想值扣完約163.84
#define GYRO_X_FIX 3
#define GYRO_Z_FIX 4


int ACCE_Y = A0;    // select the input pin for the potentiometer
int ACCE_X = A1;
int ACCE_Z = A2;
int GYRO_X = A8;// 8  ,5
int GYRO_Verf = A9;  // variable to store the value coming from the sensor  9 ,3
int GYRO_Z = A10;  //10  ,4

int sensorACCE_Y = 0;
int sensorACCE_X = 0;
int sensorACCE_Z = 0;
int sensorGYRO_X = 0;
int sensorGYRO_Verf = 0;
int sensorGYRO_Z = 0;
int filter = 1 ;

unsigned long time_old = 0;
int angle_filter_old = 0;
double time_filter = 0.11;
double sample = 0.01;  //unit: second

double radian = 0;
double angle = 0;
double angle_filter = 0;
double angle_gyro = 0;
double angle_gyro_old = 0;

void setup() {
  Serial.begin(2000000);
}

void loop() {
  char temp;

  time_old = millis();
  sensorACCE_Y = analogRead(ACCE_Y);
  sensorACCE_X = analogRead(ACCE_X);
  sensorACCE_Z = analogRead(ACCE_Z);
  sensorGYRO_X = analogRead(GYRO_X);
  sensorGYRO_Verf = analogRead(GYRO_Verf);
  sensorGYRO_Z = analogRead(GYRO_Z);

  if (Serial.available() > 0) {
    temp = Serial.read();
    if (temp == 'q') {
      temp = Serial.read(); //clear Serial buffer
      Serial.println("hi bitch");
      while (Serial.available() == 0) {}
      filter = Serial.parseInt();
      Serial.print("pwmLval: "); Serial.println(filter);
      temp = Serial.read(); //clear Serial buffer
    }
  }
  if (filter == 0) {

    //    Serial.print(sensorACCE_X);Serial.print(", ");Serial.print(sensorACCE_Y);Serial.print(", ");Serial.println(sensorACCE_Z);
    //        Serial.print(sensorACCE_X+ACC_X_FIX);Serial.print(", ");Serial.print(sensorACCE_Y+ACC_Y_FIX);Serial.print(", ");
    //        Serial.print(sensorACCE_Z+ACC_Z_FIX);Serial.print(", ");Serial.println(bit2volt(sensorACCE_Z+ACC_Z_FIX,1.0)); // the same with: Serial.println(((sensorACCE_Z+ACC_Z_FIX)*5.0)/1024.0);//放平修正

    //    sensorACCE_Z = sensorACCE_Z + ACC_Z_FIX;
    //    sensorACCE_Y = sensorACCE_Y + ACC_Y_FIX;
    //    radian = atan2(sensorACCE_Y, sensorACCE_Z);
    //    angle = (180.0 * radian) / PI;
    //    Serial.println(angle);

    //    Serial.println(sensorGYRO_X-sensorGYRO_Verf);
    //    Serial.println(sensorGYRO_Verf-sensorGYRO_Verf);
    //    Serial.println(sensorGYRO_Z-sensorGYRO_Verf);
    //    Serial.print(sensorGYRO_X-sensorGYRO_Verf);Serial.print(", ");Serial.print(sensorGYRO_Verf-sensorGYRO_Verf);Serial.print(", ");Serial.println(sensorGYRO_Z-sensorGYRO_Verf);



    //    sensorACCE_Z = sensorACCE_Z + ACC_Z_FIX;
    //    sensorACCE_Y = sensorACCE_Y + ACC_Y_FIX;
    //    radian = atan2(sensorACCE_Y, sensorACCE_Z);
    //    angle = (180.0 * radian) / PI;
    //    Serial.print(angle); Serial.print(", ");
    //
    //    sensorGYRO_X = sensorGYRO_X - sensorGYRO_Verf;
    //    sensorGYRO_Z = sensorGYRO_Z - sensorGYRO_Verf;
    //    //    Serial.print(sensorGYRO_X+GYRO_X_FIX);Serial.print(", ");Serial.println(sensorGYRO_Z+GYRO_Z_FIX);
    //    angle_gyro = angle_gyro_old + sensorGYRO_X * ((millis() - time_old) / 1000.0);
    //    Serial.println(angle_gyro);
    //    angle_gyro_old = angle_gyro ;
  }
  else if (filter == 1) {
    sensorACCE_Z = sensorACCE_Z + ACC_Z_FIX;
    sensorACCE_Y = sensorACCE_Y + ACC_Y_FIX;
    radian = atan2(sensorACCE_Y, sensorACCE_Z);
    angle = (180.0 * radian) / PI;

    sensorGYRO_X = sensorGYRO_X - sensorGYRO_Verf;
    angle_gyro = angle_gyro_old + sensorGYRO_X * ((millis() - time_old) / 1000.0);

    //int angle = 0.9*(angle_old + sensorGYRO_X*(millis()-time_old)) + 0.1*(1000*sensorACCE_Y/sensorACCE_Z);
    double num = time_filter / sample;
    double den = 1 + (time_filter / sample);
    double a = num / den; //double a = (time_filter/sample) / (1+(time_filter/sample)); //不知為何不行
    angle_filter = a * (angle_filter_old + sensorGYRO_X * sample) + (1 - a) * (angle);
    //Serial.print(a);Serial.print(", ");
    Serial.print(angle); Serial.print(", "); Serial.print(angle_gyro); Serial.print(", "); Serial.println(angle_filter);
    angle_filter_old = angle_filter;
  }
}

template <class T1, class T2>
inline T2 bit2volt(T1 a, T2 b) { //T2:1, return intergral; T2:1.0, return float;
  return (b * (a * 5) / (b * 1024));
}
