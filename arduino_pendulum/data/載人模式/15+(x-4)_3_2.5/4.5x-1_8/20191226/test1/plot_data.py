import matplotlib.pyplot as plt
import numpy as np
#data = np.loadtxt('calibration376(PM).txt',  delimiter=',',  unpack=True) #
data = np.loadtxt('No0humantest.txt',  delimiter=',',  unpack=True) #
time = data[ 0, 30:] # from index zero start, from 31 row after
angle = data[1, 30:]
refVel = data[2, 30:]
vel_L = data[3, 30:]
vel_R = data[4, 30:]
rpm_L = data[5, 30:]
rpm_R = data[6, 30:]
vel_err_L = data[7, 30:]
vel_err_R = data[8, 30:]
vel_ctrl_L = data[9, 30:]
vel_ctrl_R = data[10, 30:]
bal_ctrl = data[11, 30:]
total_ctrl_L = data[12, 30:]
total_ctrl_R = data[13, 30:]
plt.subplot(231)
plt.plot(time, angle, color='r', linewidth=0.6)
plt.legend(labels=['now degree'])
plt.xlabel('time')
plt.ylabel('degree')
plt.subplot(232)
plt.plot(time, refVel,  color='r', linewidth=0.6, linestyle="--")
plt.plot(time, vel_L,  color='b', linewidth=0.6)
plt.plot(time, vel_R,  color='G', linewidth=0.6)
plt.legend(labels=['refVel', 'now vel_L',  'now vel_R'])
plt.xlabel('time')
plt.ylabel('rpm')
plt.subplot(233)
plt.plot(time, vel_L,  color='r', linewidth=0.6)
plt.plot(time, vel_R,  color='b', linewidth=0.6)
plt.plot(time, rpm_L,  color='r', linewidth=0.6,  linestyle="--")
plt.plot(time, rpm_R,  color='b', linewidth=0.6,  linestyle="--")
plt.legend(labels=['now vel_L',  'now vel_R',  'now rpm_L',  'now rpm_R'])
plt.xlabel('time')
plt.ylabel('rpm')
plt.subplot(234)
plt.plot(time, vel_err_L,  color='r', linewidth=0.6)
plt.plot(time, vel_err_R,  color='b', linewidth=0.6)
plt.legend(labels=['vel_err_L',  'vel_err_R'])
plt.xlabel('time')
plt.ylabel('rpm')
plt.subplot(235)
#plt.plot(time, 24.0*vel_ctrl_L/390.0,  color='r', linewidth=0.6)
#plt.plot(time, 24.0*vel_ctrl_R/390.0,  color='b', linewidth=0.6)
plt.plot(time, vel_ctrl_L,  color='r', linewidth=0.6)
plt.plot(time, vel_ctrl_R,  color='b', linewidth=0.6)
plt.legend(labels=['vel_ctrl_L',  'vel_ctrl_R'])
plt.xlabel('time')
plt.ylabel('pwm')
plt.subplot(236)
#plt.plot(time, 24.0*bal_ctrl/390.0,  color='r', linewidth=0.6)
plt.plot(time, bal_ctrl,  color='r', linewidth=0.6)
plt.legend(labels=['bal_ctrl'])
plt.xlabel('time')
plt.ylabel('pwm')
plt.figure(2)
plt.subplot(111)
plt.plot(time, 24.0*total_ctrl_L/390.0,  color='r', linewidth=0.6)
plt.plot(time, 24.0*total_ctrl_R/390.0,  color='r', linewidth=0.6)
#plt.plot(time, total_ctrl_L,  color='r', linewidth=0.6)
#plt.plot(time, total_ctrl_R,  color='r', linewidth=0.6)
plt.legend(labels=['total_ctrl_L',  'total_ctrl_R'])
plt.xlabel('time')
plt.ylabel('voltage')
#plt.ylabel('pwm')
plt.show()
