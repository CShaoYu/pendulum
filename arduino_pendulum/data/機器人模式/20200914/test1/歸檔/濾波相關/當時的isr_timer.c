ISR(TIMER1_OVF_vect) {
  TCNT1 = (65535 - 1249); ////重新裝載  5ms
  cnt += 1;   //第一次執行=1
  cnt2 += 1;
  cnt3 += 5;
  cnt_time += 5;
  filter();
/*  
  if (flag_pid_balance == 1) {
    if (cnt3 >= 150) {  //換BLD 300B驅動後 啟動TIMER時加速度計會有很大的值  故等一會兒熱機一下 可能是電路有啥干擾八, 150ms

      FSMC_calculate(&refAngle, &(angle->angle_filter), pleft_balance);
      double sync_L, sync_R;
    
      if (pleft_balance->controlVal < 0)
      {
        forward();
        OCR3A = abs((int)pleft_balance->controlVal);
        TCCR3A |= _BV(COM3A1);
        OCR4A = abs((int)pleft_balance->controlVal);
        TCCR4A |= _BV(COM4A1);
      }
      else
      {
        backward();
        OCR3A = abs((int)pleft_balance->controlVal);
        TCCR3A |= _BV(COM3A1);
        OCR4A = abs((int)pleft_balance->controlVal);
        TCCR4A |= _BV(COM4A1);
      }

    }
  }
  else if (flag_constant == 1) {
    pwm_const();
    if (flag_pulse_ovf_L) {
      vel_L = 0;
      pleft_velocity->integral = 0.0;
      flag_pulse_ovf_L = 0;
    }
    else {
      if (( vel_L_tmp == 0) || ( vel_L_tmp == 1) || ( vel_L_tmp <= 230)) {
        vel_L = vel_L; //vel_L = 0;
      }
      else vel_L = 9375.00 / vel_L_tmp;   // 60 / ( (1/16000000)*8*200*64 ) = 9375,  rpm = 9375/pulse
    }

    if (flag_pulse_ovf_R) {
      vel_R = 0;
      pright_velocity->integral = 0.0;
      flag_pulse_ovf_R = 0;
    }
    else {
      if (( vel_R_tmp == 0) || ( vel_R_tmp == 1) || ( vel_R_tmp <= 230)) {
        vel_R = vel_R; //vel_R = 0;
      }
      else  vel_R = 9375.00 / vel_R_tmp;     // 60 / ( (1/16000000)*8*200*64 ) = 9375,  rpm = 9375/pulse
    }

    odm_L = (dis_L_tmp / 62.0) * 24.13;
    odm_R = (dis_R_tmp / 62.0) * 24.13;

    if ((odm_L >= refOdm) || (odm_R >= refOdm))
    {
      OCR3A = 0;  //for testing
      TCCR3A |= _BV(COM3A1);
      OCR4A = 0;
      TCCR4A |= _BV(COM4A1);
      locating = 1;
    }

    //if (( vel_L_tmp == 0) || ( vel_L_tmp == 1)) {} //vel_L = 0;
    //else vel_L = 9375.00 / vel_L_tmp;   // 60 / ( (1/16000000)*8*200*64 ) = 9375,  rpm = 9375/pulse

    //if (( vel_R_tmp == 0) || ( vel_R_tmp == 1)) {} //vel_R = 0;
    //else  vel_R = 9375.00 / vel_R_tmp;     // 60 / ( (1/16000000)*8*200*64 ) = 9375,  rpm = 9375/pulse
  }
*/
  if (cnt2 == 200) { //今天走其他case還是得歸零，所以才保留程式碼, 避免都使用cnt1造成想看快一點（每0.1、0.01...print）時，會影響 每一秒才收轉速這個機制
    cnt2 = 0;
    rpm_L = (rpm_L_tmp / 64.0) * 60.0;
    rpm_R = (rpm_R_tmp / 64.0) * 60.0;
    rpm_L_tmp = 0x000000000000000;
    rpm_R_tmp = 0x000000000000000;
    cnt3 = 150;
  }

  odm_L = (dis_L_tmp / 62.0) * 24.13;
  odm_R = (dis_R_tmp / 62.0) * 24.13;
  if ((odm_L >= refOdm) || (odm_R >= refOdm))
  {
    locating = 1;
  }

  flag_acgy = 1;
  if (cnt == 1) {
    flag_print = 1;
    cnt = 0;
  }
}


ISR(TIMER2_OVF_vect) {

  TCNT2 = (255 - 199);

  if (pulse_L >= 9375) {
    //pulse_L = 30000;
    flag_pulse_ovf_L = 1;
    pulse_L = 0;
    switch_count_pulse_L = 0;
  }
  else pulse_L += 1;

  if (pulse_R >= 9375) {
    //pulse_R = 30000;
    flag_pulse_ovf_R = 1;
    pulse_R = 0;
    switch_count_pulse_R = 0;
  }
  else pulse_R += 1;
}


//ISR(TIMER5_OVF_vect) {
//
//  TCNT5 = (65535 - 199);
//  pulse_R += 1;
//}