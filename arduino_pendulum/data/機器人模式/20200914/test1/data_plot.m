clc ; clear; close all;  
sample_time = 0.05; 

a =load('data.txt');
time = a(:,1);   %列行列行
offset = time(60);
time = time(60:end);
time = time - offset;
angle = a(:,2);
angle = angle(60:end);
plot(time, angle, 'r');
title('angle');
savefig('calibration.fig');  %若之後想再更改font size或是曲線顏色等等, 可直接編輯fig檔, 故也存
saveas(gcf,'calibration.png');  %沒上一步也可输出成png

figure
ctrl = a(:,6);
ctrl = ctrl(60:end);
plot(time, ctrl, 'r');
title('ctrl');

figure
S = a(:,7);
S = S(60:end);
plot(time, S, 'r');
title('S');

figure
omega = a(:,4);
omega = omega(60:end);
plot(time, omega, 'r');
title('angular');

figure
omega_rc = a(:,5);
omega_rc = omega_rc(60:end);
plot(time, omega_rc, 'r');
title('angular_rc');

