clc ; clear; close all;  
sample_time = 0.05; 

a =load('sensor_data.txt');
time = a(:,1);   %列行列行
offset = time(60);
time = time(60:end);
time = time - offset;
angle_filter = a(:,2);
angle_filter = angle_filter(60:end);
angle_acc = a(:,3);
angle_acc = angle_acc(60:end);
angle_gyro = a(:,4);
angle_gyro = angle_gyro(60:end);
figure
hold on; grid on;
plot(time, angle_filter, 'r');
plot(time, angle_acc, 'b');
plot(time, angle_gyro, 'g');
legend('angle filter', 'angle acc', 'angle gyro');
xlabel('time(sec)'); ylabel('deg');
title('angle compare');
savefig('angle compare.fig');  %若之後想再更改font size或是曲線顏色等等, 可直接編輯fig檔, 故也存
saveas(gcf,'angle compare.png');  %沒上一步也可输出成png

angular_velocity = a(:,5);
angular_velocity = angular_velocity(60:end);
angular_velocity_rc = a(:,6);
angular_velocity_rc = angular_velocity_rc(60:end);

figure
hold on; grid on;
plot(time, angular_velocity, 'r');
plot(time, angular_velocity_rc, 'b');
legend('angular velocity', 'angular velocity rc');
xlabel('time(sec)'); ylabel('deg/s');
title('angular velocity compare');
savefig('angular velocity compare.fig');  %若之後想再更改font size或是曲線顏色等等, 可直接編輯fig檔, 故也存
saveas(gcf,'angular velocity compare.png');  %沒上一步也可输出成png

