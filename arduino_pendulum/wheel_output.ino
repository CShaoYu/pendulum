void forward(void)
{
  PORTB |= 0b00001000;  //digitalWrite(dir_L, HIGH);
  PORTB &= 0b11111101;  //digitalWrite(dir_R, LOW);
  flag_direction = Direction::Forward;
}

void backward(void)
{
  PORTB &= 0b11110111;  //digitalWrite(dir_L, LOW);
  PORTB |= 0b00000010;  //digitalWrite(dir_R, HIGH);
  flag_direction = Direction::Backward;
}
