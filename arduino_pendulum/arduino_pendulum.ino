#define NOP __asm__ __volatile__ ("nop\n\t")

///////Left BLDC Driver Configuration///////
#define ha_L 19
#define hb_L 20
#define hc_L 21
#define dir_L 50 //PB3
#define pwm_L 5 //4, PG5, OC0B -> 5, PE3, OC3A
volatile double rpm_L = 0x000000000000000; //每1秒取的 or 每0.1秒取的 or ...
//double rpm_L_double = 0.0;  //要改成比1秒控速更小在打 到時候引數估計是 rpm/6/10 rpm/6/100 ... 且需再另外宣告一浮點數變數
volatile int rpm_L_tmp = 0x000000000000000;  //每次歸零在累加的
volatile int dis_L_tmp = 0;
volatile int vel_L_tmp = 0x000000000000000;
volatile double vel_L = 0x000000000000000;
volatile double odm_L = 0.0;
volatile double vel_old_L = 0x000000000000000;
volatile double acc_L = 0x000000000000000;

///////Rightt BLDC Driver Configuration///////
#define ha_R 2
#define hb_R 3
#define hc_R 18
#define dir_R 52 //PB1
#define pwm_R 6 //5, PE3, OC3A -> 6, PH3, OC4A
volatile double rpm_R = 0x000000000000000;
volatile int rpm_R_tmp = 0x000000000000000;  //每次歸零在累加的
volatile int dis_R_tmp = 0;
volatile int vel_R_tmp = 0x000000000000000;
volatile double vel_R = 0x000000000000000;
volatile double odm_R = 0.0;
volatile double vel_old_R = 0x000000000000000;
volatile double acc_R = 0x000000000000000;

typedef struct BLDC {
  //BLDC() {};
  double controlVal ;
  double controlVal_old ; //避免nan 原因未知  不是運算引起的  要馬達產生轉動才會有nan
  double refVal;
  double nowVal;
  double error;
  double controlMax;
  bool LR;

  double paramPid[3];  //kp, ki, kd
  double integral;
  double integralMax;

  double fuzzy_input_rule[11];
  double rule[11];
  double fsmcvector[11];

  double lamda;
  double lamda_d;
  double lamda_dd;
  double gs;
  double gu;
  double sliding_value;
};
//BLDC *pavg_velocity = new BLDC[1];  //估計只要叫pleft就好 因為應該不會同時用pid fsmc控20191023; pleft_pid改為plet_vecolity因為結構內含PID和FSMC故只要區分控速 平衡 控速及平衡就好20191028
//BLDC *pleft_balance = new BLDC[1]; //不宣告成一個balance是為了預留之後左右轉也要維持平衡

BLDC pleft_balance = {  // initVariable()也要記得改
  0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0,
  {0.0, 0.0, 0.0}, 0.0, 0.0,
  //  {-1.0, -0.875, -0.75, -0.625, -0.10, 0.0, 0.20, 0.425, 0.6, 0.775, 0.9}, // 10
  //  { -1.0, -0.85, -0.7, -0.55, -0.42, 0.0, 0.5, 0.65, 0.75, 0.85, 1.0 }, // 50
  /*  g1  不一定都會出現
    {-0.6, -0.45, -0.35, -0.3, -0.10, 0.0, 0.20, 0.425, 0.6, 0.775, 0.9}, // 10
    { -1.0, -0.8, -0.7, -0.45, -0.26, 0.0, 0.3125, 0.41, 0.47, 0.53, 0.63 }, // 80
  */
  //err, deg
  // -3.0    -2.0   -1.33   -1.0   -0.33  0.0  0.66  1.42   2.0  2.31   3.0
  { -0.9,   -0.6,  -0.399, -0.3,  -0.10, 0.0, 0.20, 0.425, 0.6, 0.693, 0.9}, // 10  g2
  { -0.8,   -0.65, -0.55,  -0.45, -0.26, 0.0, 0.3125, 0.35, 0.4, 0.45, 0.6 }, // 80        g2
  {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
  3.0, 0.2, 0.001, 10.0, 80.0, 0.0
};

BLDC pavg_velocity = {  // initVariable()也要記得改
  0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0,
  {0.0, 0.0, 0.0}, 0.0, 0.0,
  { -0.36, -0.28, -0.2, -0.12, -0.04, 0.0, 0.04, 0.12, 0.2, 0.28, 0.36}, 
  {-0.1, -0.05, 0.05, 0.2, 0.35, 0.50, 0.525, 0.55, 0.575, 0.6, 0.65 },
  {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
  1.0, 0.0, 0.0, 50.0, 1.0, 0.0
};

BLDC pavg_odometry = {  // initVariable()也要記得改
  0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0,
  {0.0, 0.0, 0.0}, 0.0, 0.0,
//err, cm
//-200, -140, -60, -20, -10, 0, 10, 20, 60, 140, 200
  { -1.0, -0.7, -0.3, -0.1, -0.05, 0.0, 0.05, 0.1, 0.3, 0.7, 1.0},
//  {0.10, 0.3, 0.35, 0.4, 0.45, 0.50, 0.55, 0.6, 0.65, 0.7, 0.75 },
  {-0.1, 0.0, 0.1, 0.2, 0.25, 0.50, 0.55, 0.6, 0.65, 0.7, 0.75 },
  {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
  1.0, 1.0, 0.0, 200.0, 1.0, 0.0
};
//BLDC *pavg_velocity = NULL; // 宣告完指標若沒有馬上就要記憶體空間通常會給0或NULL（空值）避免'迷途指標（wild pointer)'，delete動態配置後也需要做一次這件事情
//double *paramPidL = new double[3];

volatile double refAngle = 0.5;
volatile double refVel = 18.0; //rpm, 換算成m/s: 20*9.5*2.54*pi*0.01/60 = 0.12634...., 走10秒約227.4197001cm
volatile double velAngle = 0.0;  //veloctiy mode時所追的角度
volatile double refOdm = 227.4197001;  //cm, 18rpm走10秒的大概距離
volatile bool locating  = 0;

bool if_hall_error = 0; //have error : 1
bool flag_display = 0;
bool flag_enMotor = 0;
bool flag_sine = 0;
bool flag_constant = 0;
bool flag_pid_velocity = 0;
bool flag_pid_balance = 0;
bool flag_pid_velocity_balance = 0;
bool flag_display_type = 0;
volatile int flag_direction = 0; // 0:forward, 1:backward
volatile bool flag_print = 0;  //命名有點不太好 他算是啟動timer1和關閉timer1
volatile bool setZero = 0; //for pwm sin tpye scnt assgin 0

int pwmBase = 15;
int pwmVal = 0;  //for constant pwm type
volatile unsigned int cnt = 0; //for print, 每0.005s就重置
volatile unsigned int cnt2 = 0;
volatile unsigned int cnt3 = 0; //for balance, 每0.005s的控制頻率
volatile unsigned int cnt4 = 0; //for velocity, 每0.2s的控制頻率, 0.2是為了可解析速度
volatile unsigned int cnt5 = 0; //for 平衡模式5秒->速度模式5秒->平衡模式到end
volatile unsigned long cnt_time = 0; //for print total time, 每0.005s, 共可存 0.005 * 4,294,967,295 = 21,474,836.475s = 357,913.94125 min
volatile double scnt = 0 ;
volatile double swave = 0;
volatile double temp_swave, temp_swave2 = 0;

///////isr adc vect///////
//ACC_Y:A0, ACC_X:A1, ACC_Z:A2, gyro_x:A8, gyro_verf:A9, gyro_z:A10
#define ACC_Y_FIX -382 // -382 -> -375  10/11突然發生 ????????
#define ACC_X_FIX -349
#define ACC_Z_FIX -295
#define GYRO_X_FIX 3
#define GYRO_Z_FIX 4

typedef struct Sensor {
  //sensor() {};
  volatile int acce_y;  //再這初始歸零似乎沒用
  volatile int acce_x;
  volatile int acce_z;
  volatile int gyro_x;
  volatile int gyro_verf;
  volatile int gyro_z;
};
Sensor* sensor = new Sensor[1];  //Sensor* sensor = new Sensor[0]; 會出事理論上應該不會才對，不知道記憶體位址發生甚麼事 20191023

typedef struct ANGLE {
  //angle() {};
  volatile double angle_acc;
  volatile double angle_gyro;
  volatile double angle_filter;
  volatile double angle_gyro_old;
  volatile double angle_filter_old;
};
ANGLE* angle = new ANGLE[1];  //Sensor* sensor = new Sensor[0]; 會出事理論上應該不會才對，不知道記憶體位址發生甚麼事 20191023

bool sw_acgy = 1; //switch read acc, gyro
double radian = 0;
double time_filter = 0.05; // %  感覺像數字愈大愈高頻率波
double sample = 0.005;  //unit: second      //0.003  0.001, 0.003 0.005
volatile int* buff_acgy = new volatile int[6];
volatile int adc_adress = 0;
volatile bool flag_acgy = 0;
volatile byte adc_channel = 0 ;

enum PwmMode {
  Sine,
  Constant,
  PidVelocity,
  FsmcVelocity,
  PidBalance,
  FsmcBalance
};

enum ControlMode {
  Velocity,
  Balance,
  BalanceRobot
};

enum Direction {
  Forward,
  Backward
};

void (* resetFunc)(void) = 0;

//volatile unsigned long pulse_L = 0; //4,294,967,295, (2^32 - 1).
volatile int pulse_L = 0;  //-32,768 to 32,767
volatile int pulse_R = 0;
volatile int vel_L_tmp_start = 0;
volatile int vel_R_tmp_start = 0;
volatile int switch_count_pulse_L = 0;
volatile int switch_count_pulse_R = 0;

volatile double angular_velocity = 0.0;
volatile double angular_velocity_old = 0.0;
volatile double angular_velocity_rc = 0.0;

//只用在載人的變數
volatile int handle_bar = 0;
volatile bool if_handle_bar = 0;

volatile double gyro = 0;
volatile double tmd = 0.0;
volatile bool flag_bal = 0;
volatile bool flag_bal_forward = 0;
volatile double total_ctrl_L;
volatile double total_ctrl_R;

volatile double avg_odm = 0.0;  //命名pos有點像追馬達某角度, 故命名odm, odometry
volatile double avg_vel = 0.0;
volatile double avg_acc = 0.0;

void setup() {

  //DDRG |= (1 << 1); //digital pin40, PG1

  initVariable();
  setSampleTime();
  setPwmInit();

  Serial.begin(500000);
  Serial.println(pavg_velocity.controlVal);
  Serial.println(pavg_velocity.paramPid[0]);
  Serial.println(pavg_velocity.paramPid[0]);
  Serial.println(pavg_velocity.paramPid[1]);
  Serial.println(pavg_velocity.paramPid[2]);

  double *ppp = &pavg_velocity.rule[0];
  Serial.println(*(ppp + 1));
  Serial.println(ppp[1]);

  pinMode(pwm_L, OUTPUT);
  pinMode(dir_L, OUTPUT);
  analogWrite(pwm_L, 0);
  PORTB |= 0b00001000;  //digitalWrite(dir_L, HIGH);  //default forward
  pinMode(hc_L, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(hc_L), hall_L, CHANGE); //FALLING LED亮 digitalRead = LOW

  pinMode(pwm_R, OUTPUT);
  pinMode(dir_R, OUTPUT);
  analogWrite(pwm_R, 0);
  PORTB &= 0b11111101;  //digitalWrite(dir_R, LOW);
  pinMode(hb_R, INPUT_PULLUP); //INPUT_PULLUP
  attachInterrupt(digitalPinToInterrupt(hb_R), hall_R, CHANGE);

  //pinMode(hc_R, INPUT_PULLUP); //INPUT_PULLUP
  //attachInterrupt(digitalPinToInterrupt(hc_R), hall_R, CHANGE);

  flag_direction = Direction::Forward;

  //pinMode(48, OUTPUT);
  //digitalWrite(48, HIGH);
  //readHall_L(); readHall_R();

  Serial.println("Enter any key for start process"); //按任意按鍵以開始程序
  while (Serial.available() == 0) {}
  Serial.println("Process will go after 1s");
  delay(1000);

}

void loop() {
  char temp;
  if ( !if_hall_error ) {
    if (Serial.available() > 0) {
      temp = Serial.read();
      if (temp == 'q') {  //ros, set_ref_pwmVal_and_RPM_int
        //set constant pwm
        Serial.println("set pwmVal");
        temp = Serial.read(); //clear Serial buffer
        while (Serial.available() == 0) {}
        pwmVal = Serial.parseInt();
        Serial.print("pwmVal: "); Serial.println(pwmVal);
        temp = Serial.read(); //clear Serial buffer
        //set reference rpm
        Serial.println("set rpm_Lval");
        temp = Serial.read(); //clear Serial buffer
        while (Serial.available() == 0) {}
        pavg_velocity.refVal = Serial.parseInt();   //the same with pavg_velocity->refVal = 90;
        Serial.print("rpm_Lval: "); Serial.println(pavg_velocity.refVal);
        temp = Serial.read(); //clear Serial buffer
      }
      else if (temp == 'w') {  //ros, set_direction
        Serial.println("set direction of constant pwm type, f:set forward, b:set backwawrd");
        temp = Serial.read(); //clear Serial buffer
        while (Serial.available() == 0) {}
        temp = Serial.read();
        if (temp == 'f') {
          PORTB |= 0b00001000;  //digitalWrite(dir_L, HIGH);
          PORTB &= 0b11111101;  //digitalWrite(dir_R, LOW);
          flag_direction = Direction::Forward;
          Serial.println("forward");
        }
        else if (temp == 'b') {
          PORTB &= 0b11110111;  //digitalWrite(dir_L, LOW);
          PORTB |= 0b00000010;  //digitalWrite(dir_R, HIGH);
          flag_direction = Direction::Backward;
          Serial.println("backward");
        }
        else {
          Serial.println("hoops, somthings happend!");
        }
        temp = Serial.read(); //clear Serial buffer
      }
      else if (temp == 'e') {  //ros, set_pwm_mode
        Serial.println("set pwm type, 0:set pwm sine, 1:set pwm constant, 2:set pwm pid_velocity, 3:set pwm pid_balance");
        temp = Serial.read(); //clear Serial buffer
        while (Serial.available() == 0) {}
        temp = Serial.read();
        if (temp == '0') {
          flag_sine = 1;
          //若不停下來更改模式的話想要動態改模式需要做至個步驟改掉flag確保不受干擾
          flag_constant = 0;
          flag_pid_velocity = 0;
          flag_pid_balance = 0;
          flag_pid_velocity_balance = 0;
          Serial.println("pwm_sine type is set");
        }
        else if (temp == '1') {
          flag_constant = 1;
          flag_sine = 0;
          flag_pid_velocity = 0;
          flag_pid_balance = 0;
          flag_pid_velocity_balance = 0;
          Serial.println("pwm_constant type is set");
        }
        else if (temp == '2') {
          flag_pid_velocity = 1;
          flag_sine = 0;
          flag_constant = 0;
          flag_pid_balance = 0;
          flag_pid_velocity_balance = 0;
          Serial.println("pwm_pid_velocity type is set");
        }
        else if (temp == '3') {
          flag_pid_balance = 1;
          flag_sine = 0;
          flag_constant = 0;
          flag_pid_velocity = 0;
          flag_pid_velocity_balance = 0;
          Serial.println("pwm_pid_balance type is set");
        }
        else if (temp == '4') {
          flag_pid_velocity_balance = 1;
          flag_bal = 1;
          flag_bal_forward = 0;
          flag_sine = 0;
          flag_constant = 0;
          flag_pid_balance = 0;
          flag_pid_velocity = 0;
          Serial.println("pwm_pid_velocity_balance type is set");
        }
        else if (temp == '5') {
          flag_pid_velocity_balance = 1;
          flag_bal_forward = 1;
          flag_bal = 0;
          flag_sine = 0;
          flag_constant = 0;
          flag_pid_balance = 0;
          flag_pid_velocity = 0;
          Serial.println("pwm_pid_velocity_balance type is set");
        }
        temp = Serial.read(); //clear Serial buffer
      }
      else if (temp == 'd') {  //ros, switch_display_data
        flag_display = 1;
        Serial.println(flag_display);
        Serial.println("switch display data, 0:set display acc&gyro, 1:set display pwm");
        temp = Serial.read(); //clear Serial buffer
        while (Serial.available() == 0) {}
        temp = Serial.read();
        if (temp == '0') {
          flag_display_type = 0;
          Serial.println("display type is acc & gyzo");
        }
        else if (temp == '1') {
          flag_display_type = 1;
          Serial.println("display type is pwm");
        }
        temp = Serial.read(); //clear Serial buffer
      }
      else if (temp == 'r') {  //ros, reset
        Serial.println("reset after 5s");
        delay(5000);
        resetFunc();
        temp = Serial.read(); //clear Serial buffer
      }
      else if (temp == 't') {
        Serial.print("now left pid parameter: ");
        Serial.print(pavg_velocity.paramPid[0]); Serial.print(", ");
        Serial.print(pavg_velocity.paramPid[1]); Serial.print(", ");
        Serial.println(pavg_velocity.paramPid[2]);
        Serial.println("set pid parameter kp ki kd for left motor, input type:kp ki kd ");
        temp = Serial.read(); //clear Serial buffer
        while (Serial.available() == 0) {}
        for (int i = 0; i < 3; i++) {
          pavg_velocity.paramPid[i] = Serial.parseFloat();
          Serial.println(pavg_velocity.paramPid[i]); //*(paramPid+i)
        }
        temp = Serial.read(); //clear Serial buffer
      }
      else if (temp == 'o') {  //ros, set_ref_odm_float
        Serial.println("set refOdm");
        temp = Serial.read(); //clear Serial buffer
        while (Serial.available() == 0) {}
        refOdm = Serial.parseFloat();
        Serial.print("refOdm: "); Serial.println(refOdm);
        temp = Serial.read(); //clear Serial buffer
      }
      else if (temp == 's') {  //ros, switch_motor_enable

        Serial.println("switch motor enable!"); //有一個奇怪的事情，每次切換時scnt=0.01時的swave值都不太一樣(0.36~0.84), #1, 先暫時不理會 20191007
        flag_enMotor = !flag_enMotor;
        if (flag_enMotor) {
          cli();
          setAnaReadInit();
          rpm_L = 0x000000000000000; rpm_L_tmp = 0x000000000000000; dis_L_tmp = 0;
          rpm_R = 0x000000000000000; rpm_R_tmp = 0x000000000000000; dis_R_tmp = 0;
          sei();
          Serial.println("Motor go !");
          TIMSK1 = 0x01; //致能Timer 1 的overflow
          TIMSK2 = 0x01;
        }
        else {
          //pwmVal = 0; scnt = 0.0; swave = 0.0; temp_swave = 0.0; temp_swave2 = 0.0; //歸零後,ISR內同樣的變數無法再更新此value,將變數改指標的方式還沒try過,因為我懶
          ADCSRA = _BV(ADATE) | _BV(ADIE) | ~_BV(ADEN) | _BV(ADPS2) | _BV(ADPS1) | _BV(ADPS0);
          cnt3 = 0; //time
          angle->angle_acc = 0; angle->angle_gyro = 0; angle->angle_filter = 0;
          setZero = 1;
          flag_sine = 0; flag_constant = 0; flag_pid_velocity = 0; flag_pid_balance = 0; flag_pid_velocity_balance = 0;
          flag_display = 0;
          Serial.println("flag:sine, constant, pid, display = 0");
          OCR3A = 0; //analogWrite(pwm_L, 0);
          TCCR3A |= _BV(COM3A1);
          OCR4A = 0; //analogWrite(pwm_R, 0);
          TCCR4A |= _BV(COM4A1);
          initVariable();

          cli();
          TIMSK1 = 0x00; //關閉Timer 1 的overflow 致能
          TIMSK2 = 0x00;
          sei();
          pulse_L = 0;
          pulse_R = 0;
          switch_count_pulse_L = 0;
          switch_count_pulse_R = 0;

          flag_bal_forward = 0;
          flag_bal = 0;
          flag_bal_forward = 0;

          cnt_time = 0;
          angle->angle_filter_old = 0.0;

          odm_L = 0.0;
          odm_R = 0.0;
          locating = 0;
          cnt4 = 0;
          cnt5 = 0;
          refAngle = 0.5;
          pleft_balance.lamda_d = 0.2;

          Serial.println("Motor stop !");
        }
        temp = Serial.read(); //clear Serial buffer
      }
      else {
        Serial.println("no this command");
      }
    }
    if (flag_print == 1) {
      if (flag_display) {
        if (flag_display_type == 0) {
          /*
            Serial.print(cnt_time / 1000.0, 3); Serial.print(", ");
            Serial.print(angle->angle_acc); Serial.print(", "); Serial.print(angle->angle_gyro); Serial.print(", ");  Serial.print(angle->angle_filter);
            Serial.print(", "); Serial.print(sensor->gyro_verf); Serial.print(", "); Serial.print(sensor->gyro_x); Serial.print(", ");
            Serial.println(-(sensor->gyro_x - sensor->gyro_verf)); //頭往上是正
          */
          Serial.print(cnt_time / 1000.0, 3); Serial.print(", ");
          Serial.print(angle->angle_filter); Serial.print(", "); // deg
          Serial.print(angle->angle_acc, 4); Serial.print(", "); // deg
          Serial.print(angle->angle_gyro, 4); Serial.print(", "); // deg
          Serial.print(angular_velocity, 4); Serial.print(", "); // deg/s
          Serial.println(angular_velocity_rc, 4); // deg/s, RC
        }
        else if (flag_display_type == 1) { //0 1 2 3之後可能可以改完enum用名子代替數字提高可讀性
          if (flag_sine == 1) {
            Serial.print(scnt); Serial.print(", "); Serial.print(swave); Serial.print(", ");
            if (scnt <= PI) {
              Serial.print(temp_swave); //再確認swave是否等於temp_swave, 有點疑心病的check
            }
            else if (scnt > PI) {
              Serial.print(temp_swave2);
            }
            Serial.print(", ");
            Serial.print(rpm_L_tmp); Serial.print(", ");
            Serial.println(rpm_R_tmp);
          }
          else if (flag_constant == 1) {

            Serial.print(cnt_time / 1000.0, 3); Serial.print(", ");
            Serial.print(angle->angle_filter); Serial.print(", ");
            Serial.print(pwmVal); Serial.print(", ");
            Serial.print(rpm_L, 3); Serial.print(", "); Serial.print(rpm_R, 3); Serial.print(", ");
            //Serial.print(rpm_L_tmp, 3); Serial.print(", "); Serial.print(rpm_R_tmp); Serial.print(", "); //保留show temp rpm方便檢驗或爾目前是否讀值正常
            Serial.print(dis_L_tmp); Serial.print(", "); Serial.print(dis_R_tmp); Serial.print(", ");
            Serial.print(vel_L); Serial.print(", "); Serial.print(vel_R); Serial.print(", ");
            Serial.print(odm_L); Serial.print(", "); Serial.print(odm_R); Serial.print(", "); Serial.println(locating);
            //Serial.print(vel_L_tmp); Serial.print(", "); Serial.print(vel_R_tmp); Serial.print(", ");
            //Serial.print(switch_count_pulse_L); Serial.print(", "); Serial.println(switch_count_pulse_R);
            //Serial.println(pulse_L);

          }
          else if (flag_pid_balance == 1) {
            Serial.print(cnt_time / 1000.0, 3); Serial.print(", ");
/*************純平衡控制*************/
//            Serial.print(angle->angle_filter); Serial.print(", ");
//            Serial.print(angular_velocity_rc); Serial.print(", ");
//            Serial.print(pleft_balance.controlVal); Serial.print(", ");
//            Serial.print(pleft_balance.error); Serial.print(", ");
//            Serial.print(vel_L); Serial.print(", ");
//            Serial.print(vel_R); Serial.print(", ");
//            Serial.print(flag_direction); Serial.print(", ");
//            Serial.println(pleft_balance.sliding_value);
/*************平衡模式5秒->速度模式5秒->平衡模式到end*************/
            Serial.print(angle->angle_filter); Serial.print(", ");
            Serial.print(angular_velocity_rc); Serial.print(", ");
            Serial.print(pavg_velocity.controlVal); Serial.print(", ");
            Serial.print(pleft_balance.controlVal); Serial.print(", ");
            Serial.print(vel_L); Serial.print(", ");
            Serial.print(vel_R); Serial.print(", ");
            Serial.print(acc_L); Serial.print(", ");
            Serial.print(acc_R); Serial.print(", ");
            Serial.print(pleft_balance.sliding_value); Serial.print(", ");
            Serial.println(pavg_velocity.sliding_value);
/*************平衡模式5秒->位置控制->平衡模式到end*************/
//            Serial.print(angle->angle_filter); Serial.print(", ");
//            Serial.print(angular_velocity_rc); Serial.print(", ");
//            Serial.print(pavg_odometry.controlVal); Serial.print(", ");
//            Serial.print(pleft_balance.controlVal); Serial.print(", ");   
//            Serial.print(odm_L); Serial.print(", ");
//            Serial.print(odm_R); Serial.print(", ");
//            Serial.print(vel_L); Serial.print(", ");
//            Serial.print(vel_R); Serial.print(", ");                     
//            Serial.print(pleft_balance.sliding_value); Serial.print(", ");
//            Serial.println(pavg_odometry.sliding_value);
          }
          else if (flag_pid_velocity_balance == 1) {
            Serial.print(cnt3 / 1000.0); Serial.print(", ");
            Serial.print(vel_L); Serial.print(", "); Serial.print(vel_R); Serial.print(", ");
            Serial.print(rpm_L, 3); Serial.print(", "); Serial.print(rpm_R, 3); Serial.print(", ");
            Serial.print(pavg_velocity.error); Serial.print(", ");
            Serial.print(pavg_velocity.controlVal); Serial.print(", ");
            Serial.print(angle->angle_filter); Serial.print(", ");
            Serial.print(pleft_balance.error); Serial.print(", ");
            Serial.print(pleft_balance.controlVal); Serial.print(", ");
            Serial.print(total_ctrl_L); Serial.print(", ");
            Serial.println(total_ctrl_R);
          }
        }
      }
      flag_print = 0;
    }
  }
}
