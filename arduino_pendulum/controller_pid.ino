template <class T1, class T2>
struct BLDC* PID_calculate( T2* refPoint, T1* prePoint,  BLDC motor, int _ControlMode) {
  double dt = sample;
  T2 error = *refPoint - *prePoint;  //double error、; double *error 後面print會錯應該是記憶體位址內的值發生未預期錯誤 原因可能和宣告指標時沒有賦與初始記憶體區塊類似
  double Pout, Iout, output;

  if (_ControlMode == ControlMode::Balance) {
    if (error > 0) {
//      mot/or.paramPid[0] = 3.0;
//      /motor.paramPid[2] = 0.010;  
      motor.paramPid[0] = 7.5;  //雙臂模式
      motor.paramPid[2] = 2.165;  //雙臂模式
    }
    else if (error < 0) {
//      mot/or.paramPid[0] = 3.0;
//      /motor.paramPid[2] = 0.015;  
      motor.paramPid[0] = 7.5;  //雙臂模式
      motor.paramPid[2] = 2.165;  //雙臂模式
    }
    Pout = motor.paramPid[0] * error;
    Iout = 0;
    gyro = - tmd;
//    output = Pout + Iout + motor.paramPid[2] * gyro;
    output = Pout + Iout + motor.paramPid[2] * (error - motor.error);
//    if (abs(error) <= 1.0 ) {
//      output = 0.0;
//    }
    if (abs(error) <= 0.5 ) {  //雙臂模式
      output = 0.0;
    }    
  }
  else if (_ControlMode == ControlMode::Velocity) {
    error = 1.3 * error;
    Pout = motor.paramPid[0] * error ;

    motor.integral = motor.integral + motor.paramPid[1] * error * dt;
    if ( motor.integral >= motor.integralMax) {
      motor.integral = motor.integralMax;
    }
    else if (motor.integral <= -motor.integralMax) {
      motor.integral = -motor.integralMax;
    }
    Iout = motor.integral;

    output = Pout + Iout ;
  }
//  else if (_ControlMode == ControlMode::BalanceRobot) {
//    if (abs(error) <= 1) {
//      integral_L = 0;
//      integral_R = 0;
//    }
//    if (error > 0) {
//      if (error > 15) {
//        motor.paramPid[0] = 5;
//        motor.paramPid[2] = 0.065;
//        motor.paramPid[1] = 6;
//      }
//      else {
//        motor.paramPid[0] = 5;
//        motor.paramPid[2] = 0.015;
//        motor.paramPid[1] = 6;
//      }
//    }
//    else if (error < 0) {
//      motor.paramPid[0] = 3;
//      motor.paramPid[2] = 0.04;
//      motor.paramPid[1] = 3;
//    }
//
//    Pout = motor.paramPid[0] * error;
//    if ( motor.LR == 0 ) {
//      integral_L = integral_L + motor.paramPid[1] * error * dt;
//      if ( integral_L >= 20) {
//        integral_L = 20;
//      }
//      else if (integral_L <= -20) {
//        integral_L = -20;
//      }
//      Iout = integral_L;
//    }
//    else {
//      integral_R = integral_R + motor.paramPid[1] * error * dt;
//      if ( integral_R >= 20) {
//        integral_R = 20;
//      }
//      else if (integral_R <= -20) {
//        integral_R = -20;
//      }
//      Iout = integral_R;
//    }
//    gyro = - tmd;
//    output = Pout + Iout + motor.paramPid[2] * gyro;
//  }


  if (output >= motor.controlMax) {
    output =  motor.controlMax;
  }
  else if (output <= -motor.controlMax) {
    output = -motor.controlMax;
  }

  motor.error = error;
  motor.controlVal = output;
  return &motor;
}

/*
  struct emp{
   int a;
  };
  struct emp e = {1};
  =
  typedef struct emp{
    int a;
  };
  emp e = {1};
*/
