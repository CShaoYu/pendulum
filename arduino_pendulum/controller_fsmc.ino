//template <class T1, class T2>
//struct BLDC* FSMC_calculate( T2* refPoint, T1* prePoint, BLDC* motor) {
struct BLDC* FSMC_calculate( double* refPoint, double* prePoint, BLDC* motor) {
/****想不起來幹啥的
  if (!motor.LR) rpm_L = *prePoint; //我看並不是每1秒再看, 而我控制是每1秒在控, 所以需要配合那1秒, 將值給存下來才會對應到每1秒的控制時當前速度是多少才會是當前速度,此離程式愈靠近愈好因為我是用指標, 此時外部中斷若還在變化這裡也會跟著變
  else rpm_R = *prePoint;
****/
  double error = *refPoint - *prePoint;
  error = -error;
  
  double error_change = angular_velocity_rc;
  double error_dd = (angular_velocity_rc - angular_velocity_old)*200.0;  // *200 = 1/0.005
  double gs = motor->gs;
  double gu = motor->gu;
  double lamda = motor->lamda;
  double lamda_d = motor->lamda_d;
  double lamda_dd = motor->lamda_dd;
  double x = 0.0;
  
//  double fuzzy_input_rule[11]={-1.0, -0.875, -0.75, -0.625, -0.10, 0.0, 0.20, 0.425, 0.6, 0.775, 0.9}; 
//  double rule[11] = { -1.0, -0.85, -0.7, -0.55, -0.42, 0.0, 0.5, 0.65, 0.75, 0.85, 1.0 };
//  delete [] motor.fsmcvector;  //不加這行執行幾秒後會整個struct object被初始化,
//  motor.fsmcvector = new double[11]();

  for (int i=0; i<11; i++)
    motor->fsmcvector[i] = 0.0;
    
  double *fuzzy_input_rule = &motor->fuzzy_input_rule[0]; 
  double *rule = &motor->rule[0];

  
//  lamda = 3.0; lamda_d = 0.2;
  long double sliding_value = lamda*error + lamda_d*error_change + lamda_dd*error_dd ;
  x=sliding_value/gs;

  if (sliding_value >= fuzzy_input_rule[10] * gs) {
    motor->fsmcvector[10] = 1.0;
  }
  else if (sliding_value >= fuzzy_input_rule[9]*gs && sliding_value <= fuzzy_input_rule[10]*gs) {
    motor->fsmcvector[10] = (x-fuzzy_input_rule[9])/(fuzzy_input_rule[10]-fuzzy_input_rule[9]);
    motor->fsmcvector[9] = 1.0-motor->fsmcvector[10];
  }
  else if (sliding_value >= fuzzy_input_rule[8]*gs && sliding_value <= fuzzy_input_rule[9]*gs) {
    motor->fsmcvector[9] = (x-fuzzy_input_rule[8])/(fuzzy_input_rule[9]-fuzzy_input_rule[8]);
    motor->fsmcvector[8] = 1.0-motor->fsmcvector[9];
  }
  else if (sliding_value >= fuzzy_input_rule[7]*gs && sliding_value <= fuzzy_input_rule[8]*gs) {
    motor->fsmcvector[8] = (x-fuzzy_input_rule[7])/(fuzzy_input_rule[8]-fuzzy_input_rule[7]);
    motor->fsmcvector[7] = 1.0-motor->fsmcvector[8];
  }
  else if (sliding_value >= fuzzy_input_rule[6]*gs && sliding_value <= fuzzy_input_rule[7]*gs) {
    motor->fsmcvector[7] = (x-fuzzy_input_rule[6])/(fuzzy_input_rule[7]-fuzzy_input_rule[6]);
    motor->fsmcvector[6] = 1.0-motor->fsmcvector[7];
  }
  else if (sliding_value >= fuzzy_input_rule[5]*gs && sliding_value <= fuzzy_input_rule[6]*gs) {
    motor->fsmcvector[6] = (x-fuzzy_input_rule[5])/(fuzzy_input_rule[6]-fuzzy_input_rule[5]);
    motor->fsmcvector[5] = 1.0-motor->fsmcvector[6];
  }
  else if (sliding_value >= fuzzy_input_rule[4]*gs && sliding_value <= fuzzy_input_rule[5]*gs) {
    motor->fsmcvector[5] = (x-fuzzy_input_rule[4])/(fuzzy_input_rule[5]-fuzzy_input_rule[4]);
    motor->fsmcvector[4] = 1.0-motor->fsmcvector[5];
  }
  else if (sliding_value >= fuzzy_input_rule[3]*gs && sliding_value <= fuzzy_input_rule[4]*gs) {
    motor->fsmcvector[4] = (x-fuzzy_input_rule[3])/(fuzzy_input_rule[4]-fuzzy_input_rule[3]);
    motor->fsmcvector[3] = 1.0-motor->fsmcvector[4];
  }
  else if (sliding_value >= fuzzy_input_rule[2]*gs && sliding_value <= fuzzy_input_rule[3]*gs) {
    motor->fsmcvector[3] = (x-fuzzy_input_rule[2])/(fuzzy_input_rule[3]-fuzzy_input_rule[2]);
    motor->fsmcvector[2] = 1.0-motor->fsmcvector[3];
  }
  else if (sliding_value >= fuzzy_input_rule[1]*gs && sliding_value <= fuzzy_input_rule[2]*gs) {
    motor->fsmcvector[2] = (x-fuzzy_input_rule[1])/(fuzzy_input_rule[2]-fuzzy_input_rule[1]);
    motor->fsmcvector[1] = 1.0-motor->fsmcvector[2];
  }
  else if (sliding_value >= fuzzy_input_rule[0]*gs && sliding_value <= fuzzy_input_rule[1]*gs) {
    motor->fsmcvector[1] = (x-fuzzy_input_rule[0])/(fuzzy_input_rule[1]-fuzzy_input_rule[0]);
    motor->fsmcvector[0] = 1.0-motor->fsmcvector[1];
  }
  else if (sliding_value <= -fuzzy_input_rule[0] * gs) {
    motor->fsmcvector[0] = 1.0;
  }

  double weight = 0.0;
  double weight_sum = 0.0;
  for (int i = 0; i < 11; i++) {
    if (motor->fsmcvector[i] > 0.0) {
      weight += motor->fsmcvector[i];
      //weight_sum += (motor.fsmcvector[i]) * rule[i];
      weight_sum = fma(motor->fsmcvector[i],rule[i],weight_sum);
    }
  }
//  if ((sliding_value < -3) && (abs(error_change) < 2.0))
//  if (sliding_value < -3.0)
//    gu = gu * (1.0/exp(sliding_value+3));  
  
  double output;
  output = (long double)(weight_sum * gu / weight);  //--2020/10/6 bugㄏㄏ  會變整數  不過實際也只能整數  所以這裡只影響plot時
  
  if (isnan(output))
    output = motor->controlVal_old;
    
  if (output >= motor->controlMax) 
    output =  motor->controlMax;
  else if (output <= -motor->controlMax) 
    output = -motor->controlMax;

  motor->controlVal_old = output;

  motor->error = error;  //the same with preError = error;  //放外面不行不知為啥
  motor->controlVal = output;
  motor->sliding_value = sliding_value;

  return motor;
}

struct BLDC* FSMC_thetaCMD_calculate( double refPoint, double prePoint, double prePointDiff, BLDC* motor) {
/****想不起來幹啥的
  if (!motor.LR) rpm_L = *prePoint; //我看並不是每1秒再看, 而我控制是每1秒在控, 所以需要配合那1秒, 將值給存下來才會對應到每1秒的控制時當前速度是多少才會是當前速度,此離程式愈靠近愈好因為我是用指標, 此時外部中斷若還在變化這裡也會跟著變
  else rpm_R = *prePoint;
****/
  double error = refPoint - prePoint;    
  error = -error;
  double error_change = prePointDiff;
  error_change = -error_change;
  double gs = motor->gs;
  double gu = motor->gu;
  double lamda = motor->lamda;
  double lamda_d = motor->lamda_d;
  double x;
  
//  double fuzzy_input_rule[11]={-1.0, -0.8, -0.6, -0.4, -0.2, 0.0, 0.2, 0.4, 0.6, 0.8, 1.0}; 
//  double rule[11] = { -1.0, -0.95, -0.86, -0.77, -0.68, 0.23, 0.68, 0.77, 0.86, 0.95, 1.0 };
//  delete [] motor.fsmcvector;  //不加這行執行幾秒後會整個struct object被初始化,
//  motor.fsmcvector = new double[11]();

  for (int i=0; i<11; i++)
    motor->fsmcvector[i] = 0.0;
    
  double *fuzzy_input_rule = &motor->fuzzy_input_rule[0]; 
  double *rule = &motor->rule[0];
  
//  lamda = 1; lamda_d = 0.0;
  double sliding_value = lamda*error + lamda_d*error_change ;
  x=double(sliding_value/gs);

  if (sliding_value >= 1.0 * gs) {
    motor->fsmcvector[10] = 1.0;
  }
  else if (sliding_value >= fuzzy_input_rule[9]*gs && sliding_value <= fuzzy_input_rule[10]*gs) {
    motor->fsmcvector[10] = (x-fuzzy_input_rule[9])/(fuzzy_input_rule[10]-fuzzy_input_rule[9]);
    motor->fsmcvector[9] = 1.0-motor->fsmcvector[10];
  }
  else if (sliding_value >= fuzzy_input_rule[8]*gs && sliding_value <= fuzzy_input_rule[9]*gs) {
    motor->fsmcvector[9] = (x-fuzzy_input_rule[8])/(fuzzy_input_rule[9]-fuzzy_input_rule[8]);
    motor->fsmcvector[8] = 1.0-motor->fsmcvector[9];
  }
  else if (sliding_value >= fuzzy_input_rule[7]*gs && sliding_value <= fuzzy_input_rule[8]*gs) {
    motor->fsmcvector[8] = (x-fuzzy_input_rule[7])/(fuzzy_input_rule[8]-fuzzy_input_rule[7]);
    motor->fsmcvector[7] = 1.0-motor->fsmcvector[8];
  }
  else if (sliding_value >= fuzzy_input_rule[6]*gs && sliding_value <= fuzzy_input_rule[7]*gs) {
    motor->fsmcvector[7] = (x-fuzzy_input_rule[6])/(fuzzy_input_rule[7]-fuzzy_input_rule[6]);
    motor->fsmcvector[6] = 1.0-motor->fsmcvector[7];
  }
  else if (sliding_value >= fuzzy_input_rule[5]*gs && sliding_value <= fuzzy_input_rule[6]*gs) {
    motor->fsmcvector[6] = (x-fuzzy_input_rule[5])/(fuzzy_input_rule[6]-fuzzy_input_rule[5]);
    motor->fsmcvector[5] = 1.0-motor->fsmcvector[6];
  }
  else if (sliding_value >= fuzzy_input_rule[4]*gs && sliding_value <= fuzzy_input_rule[5]*gs) {
    motor->fsmcvector[5] = (x-fuzzy_input_rule[4])/(fuzzy_input_rule[5]-fuzzy_input_rule[4]);
    motor->fsmcvector[4] = 1.0-motor->fsmcvector[5];
  }
  else if (sliding_value >= fuzzy_input_rule[3]*gs && sliding_value <= fuzzy_input_rule[4]*gs) {
    motor->fsmcvector[4] = (x-fuzzy_input_rule[3])/(fuzzy_input_rule[4]-fuzzy_input_rule[3]);
    motor->fsmcvector[3] = 1.0-motor->fsmcvector[4];
  }
  else if (sliding_value >= fuzzy_input_rule[2]*gs && sliding_value <= fuzzy_input_rule[3]*gs) {
    motor->fsmcvector[3] = (x-fuzzy_input_rule[2])/(fuzzy_input_rule[3]-fuzzy_input_rule[2]);
    motor->fsmcvector[2] = 1.0-motor->fsmcvector[3];
  }
  else if (sliding_value >= fuzzy_input_rule[1]*gs && sliding_value <= fuzzy_input_rule[2]*gs) {
    motor->fsmcvector[2] = (x-fuzzy_input_rule[1])/(fuzzy_input_rule[2]-fuzzy_input_rule[1]);
    motor->fsmcvector[1] = 1.0-motor->fsmcvector[2];
  }
  else if (sliding_value >= fuzzy_input_rule[0]*gs && sliding_value <= fuzzy_input_rule[1]*gs) {
    motor->fsmcvector[1] = (x-fuzzy_input_rule[0])/(fuzzy_input_rule[1]-fuzzy_input_rule[0]);
    motor->fsmcvector[0] = 1.0-motor->fsmcvector[1];
  }
  else if (sliding_value <= -1.0 * gs) {
    motor->fsmcvector[0] = 1.0;
  }

  double weight = 0.0;
  double weight_sum = 0.0;
  for (int i = 0; i < 11; i++) {
    if (motor->fsmcvector[i] > 0.0) {
      weight += motor->fsmcvector[i];
      //weight_sum += (motor->fsmcvector[i]) * rule[i];
      weight_sum = fma(motor->fsmcvector[i],rule[i],weight_sum);
    }
  }

  double output = 0.0;
  output = (long double)(weight_sum * gu / weight);  //--2020/10/6 bugㄏㄏ  會變整數  不過實際也只能整數  所以這裡只影響plot時
  if (isnan(output))
    output = motor->controlVal_old;
        
  if (output >= motor->controlMax) 
    output =  motor->controlMax;  
  else if (output <= -motor->controlMax) 
    output = -motor->controlMax;  

  motor->error = error;  //the same with preError = error;  //放外面不行不知為啥
  motor->controlVal = output;
  motor->sliding_value = sliding_value;

  motor->controlVal_old = output;  

  return motor;
}
