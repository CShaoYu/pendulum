void pwm_sine(volatile double &scnt) {
  if ((scnt > 2 * PI) or (setZero == 1)) {
    scnt = 0.0;
    setZero = 0;
  }

  if (scnt <= PI) {
    swave  = pwmVal * sin(scnt);
    temp_swave = swave;
    digitalWrite(dir_L, LOW);analogWrite(pwm_L, swave);
    digitalWrite(dir_R, HIGH);analogWrite(pwm_R, swave);
  }
  else if (scnt > PI) {
    swave  = pwmVal * sin(scnt);
    temp_swave2 = swave;
    digitalWrite(dir_L, HIGH);analogWrite(pwm_L, -swave);
    digitalWrite(dir_R, LOW);analogWrite(pwm_R, -swave);
  }
}

void pwm_const() {
  
  //analogWrite(pwm_L, pwmVal);
  //analogWrite(pwm_R, pwmVal);
  OCR3A = pwmVal;
  TCCR3A |= _BV(COM3A1);
  OCR4A = pwmVal;
  TCCR4A |= _BV(COM4A1); 
}
