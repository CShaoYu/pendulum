template <class T1, class T2>
inline T2 bit2volt(T1 a, T2 b) { //T2:1, return intergral; T2:1.0, return float;
  return (b * (a * 5) / (b * 1024));
}

void initVariable(void) {
  ///////Control balance///////
  pleft_balance.controlVal = 0;
  pleft_balance.controlVal_old = 0.0;
  pleft_balance.refVal = 0;
  pleft_balance.nowVal = 0;
  pleft_balance.error = 0;
  pleft_balance.controlMax = 80.0;
  pleft_balance.LR = 0;
  ///////PID///////
  pleft_balance.paramPid[0] = 0 ;
  pleft_balance.paramPid[1] = 0;
  pleft_balance.paramPid[2] = 0;
  pleft_balance.integral = 0;
  pleft_balance.integralMax = 0;
  ///////FSMC///////
  pleft_balance.lamda = 3.0;
  pleft_balance.lamda_d = 0.20;
  pleft_balance.lamda_dd = 0.001;
  pleft_balance.gs = 10.0;
  pleft_balance.gu = 80.0;
  pleft_balance.sliding_value = 0.0;

  ///////Control velocity///////
  pavg_velocity.controlVal = 0;
  pavg_velocity.controlVal_old = 0.0;
  pavg_velocity.refVal = 0;
  pavg_velocity.nowVal = 0;
  pavg_velocity.error = 0;
  pavg_velocity.controlMax = 50.0;
  pavg_velocity.LR = 0;  
  ///////PID///////
  pavg_velocity.paramPid[0] = 0.0;
  pavg_velocity.paramPid[1] = 0.0;  
  pavg_velocity.paramPid[2] = 0.0;
  pavg_velocity.integral = 0.0;
  pavg_velocity.integralMax = 0.0;
  ///////FSMC///////
  pavg_velocity.lamda = 1.0;
  pavg_velocity.lamda_d = 0.0;
  pavg_velocity.lamda_dd = 0.0;
  pavg_velocity.gs = 50.0;
  pavg_velocity.gu = 1.0;
  pavg_velocity.sliding_value = 0.0;

  ///////Control position///////
  pavg_odometry.controlVal = 0;
  pavg_odometry.controlVal_old = 0.0;
  pavg_odometry.refVal = 0;
  pavg_odometry.nowVal = 0;
  pavg_odometry.error = 0;
  pavg_odometry.controlMax = 50.0;
  pavg_odometry.LR = 0;  
  ///////PID///////
  pavg_odometry.paramPid[0] = 0.0;
  pavg_odometry.paramPid[1] = 0.0;  
  pavg_odometry.paramPid[2] = 0.0;
  pavg_odometry.integral = 0.0;
  pavg_odometry.integralMax = 0.0;
  ///////FSMC///////
  pavg_odometry.lamda = 1.0;
  pavg_odometry.lamda_d = 1.0;
  pavg_odometry.lamda_dd = 0.0;
  pavg_odometry.gs = 200.0;
  pavg_odometry.gu = 1.0;
  pavg_odometry.sliding_value = 0.0;
  
  angle->angle_acc = 0;
  angle->angle_gyro = 0;
  angle->angle_filter = 0;
  angle->angle_gyro_old = 0;
  angle->angle_filter_old = 0;

  sensor->acce_y = 0;
  sensor->acce_x = 0;
  sensor->acce_z = 0;
  sensor->gyro_x = 0;
  sensor->gyro_verf = 0;
  sensor->gyro_z = 0;
}

void setSampleTime() {
  cli(); //stop all interrupt
  TCCR1A = 0x00;
  TCCR1B = 0x03;  // == 0b00000011   //(0=No clock source  clk/1,clk/8,clk/64,clk/256,clk/1024) 預除器(prescaler)
                  //        1/(16M/64) = 4us
  TCNT1 = (65535 - 1249); //Timer 1 暫存器含有16-bits, Timer 2 則是 8-bits, 1次要花4us配到1ms，要250次
  TIMSK1 = 0x00;  //致能Timer1的overflow

  TCCR2A = 0x00;
  TCCR2B = 0x02;  // == 0b00000100   
                  //(0=No clock source  clk/1,clk/8,clk/32,clk/64,clk/128,clk/256,clk/1024) 預除器(prescaler)
                  //        1/(16M/8) = 0.5us
  TCNT2 = (255 - 79); //Timer 1 暫存器含有16-bits, Timer 2 則是 8-bits, 1次要花0.5us，配到100us(10kHz)，要200次, 
                      // 1次要花0.5us，配到40us(2.5kHz)，要80次  pwm100時偶爾會有變化過大的情況（平均15XX突然有2000）,50us也會 不知道是不是太慢還是甚麼頻率沒對到(
  TIMSK2 = 0x00;  //致能Timer2的overflow

  //  TCCR5A = 0x00;
  //  TCCR5B = 0x02;  // == 0b00000011   //(0=No clock source  clk/1,clk/8,clk/64,clk/256,clk/1024) 預除器(prescaler)
  //              //        1/(16M/8) = 0.5us
  //  TCNT5 = (65535 - 199); //Timer 1 暫存器含有16-bits, Timer 2 則是 8-bits, 1次要花0.5us配到100us，要200次
  //  TIMSK5 = 0x00;  //致能Timer 1 的overflow
  sei();
}

void setPwmInit() {
  //#define pwm_L 4 //4, PG5, OC0B -> 5, PE3, OC3A
  //#define pwm_R 5 //5, PE3, OC3A -> 6, PH3, OC4A

  ICR3 = 512;   //left, pin 5  //試誤法得出以此為925解析度時 100rpm以下 給同樣的pwm bit 兩邊會有同樣轉速
  TCCR3A = _BV(WGM31) ;//fast PWM mode,set new OCR value, Mode 7
  TCCR3B = _BV(WGM33) | _BV(WGM32) | _BV(CS31);  //(1/(16000000/8)) -> *1024   1/ans 約等於 1.953125kHz,  *512 3.9kHz
  //TCCR3B = _BV(WGM33) | _BV(WGM32) | _BV(CS30);  //(1/16000000) -> *1024   1/ans 約等於 15.625kHz
  //ICR3 = 255;
  //TCCR3B = _BV(WGM33) | _BV(WGM32) | _BV(CS30);  //1/(1/16000000)* 255) ,255 約62.5kHz有拿示波器看過,
  OCR3A = 0;//duty cycle
  TCCR3A |= _BV(COM3A1);//enable timer3 pwm


  ICR4 = 290;   //ICR 無2 //right, pin 6  //試誤法得出以此為290解析度時 30rpm以下 給同樣的pwm bit 兩邊會有同樣轉速, 測試方法:貼貼紙定電壓轉一陣子看貼紙誤差,
  TCCR4A = _BV(WGM41) ;//fast PWM mode,set new OCR value, Mode 7
  TCCR4B = _BV(WGM43) | _BV(WGM42) | _BV(CS41);//(1/(16000000/8)) -> *1024   1/ans 約等於 1.953125kHz, *512 3.9kHz
  //TCCR4B = _BV(WGM43) | _BV(WGM42) | _BV(CS40);  //(1/16000000) -> *1024   1/ans 約等於 15.625kHz
  //ICR4 = 255;
  //TCCR4B = _BV(WGM43) | _BV(WGM42) | _BV(CS40);  //1/(1/16000000)* 255)  約62.5 kHz
  OCR4A = 0;//duty cycle
  TCCR4A |= _BV(COM4A1);//enable timer4 pwm
}

void setAnaReadInit(void) {
  cli();
  
  //ADMUX |= (1 << ADLAR); //左移10bit內的左邊8個bit將其放在ADCH（不太清楚用意為何），選用
  ADMUX = 0b01000000;
  //設定ADC read 預除器
  //ADCSRA |= (1<<ADPS2);
  //ADCSRA |= (1<<ADPS1);
  //ADCSRA |= (1<<ADPS0);
  //ADCSRA |= (1 << ADATE); //enable auto trigger 需再往下看ADCSRB內的ADTS2：0
  //ADCSRA |= (1 << ADIE); //enable interrupts when measurement complete
  //ADCSRA |= (1 << ADEN); //enable ADC
  //111:128(示波器實際勾約為550Hz*2) ,110:64(示波器實際勾約為930Hz*2), 101:32(示波器實際勾約為1.6kHz*2), *2是因為我是xorLED做亮暗切換, 但亮跟暗都算是觸發一次此adc_vect , 111 110加速度雜訊較多，但濾波依舊強健, 在此猜測可能是慢於timer1
  //20191028下午某時不知為何改快之後加速度的雜訊就沒了, 可能要看一下code差異在那\
  //20191028,20:21, filter(）放0.001s處, Timer1放FMSC, 101 111, 角度就算filter後會有極大抖動, 將filter()放回cnt==10處, 101 111就正常了
  ADCSRA = _BV(ADATE) | _BV(ADIE) | _BV(ADEN) | _BV(ADPS2) | ~_BV(ADPS1) | _BV(ADPS0);
  ADCSRB = 0; //free Running mode
  ADCSRA |= (1 << ADSC); //start ADC measurement,set ADSC in ADCSRA, ADC Start Conversion
  //ADCSRB = (1<< MUX5);

  sei();// Enable global interrupts
}
