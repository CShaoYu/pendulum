/*
   rosserial PubSub Example
   Prints "hello world!" and toggles led
*/

#include <ros.h>
#include <std_msgs/String.h>
#include <std_msgs/Float32.h>

double hello ;


ros::NodeHandle  nh;

std_msgs::String str_msg;
ros::Publisher chatter("str_msggg", &str_msg);


void messageCb( const std_msgs::Float32 toggle_msg) {

  hello = toggle_msg.data;

}
ros::Subscriber<std_msgs::Float32> sub("command", messageCb );


void setup()
{
  nh.initNode();
  nh.advertise(chatter);
  nh.subscribe(sub);
}

void loop()
{

  if (hello == 63.87) {
    str_msg.data = "fuck";
    hello = 0;
  }
  else {
    str_msg.data = "pwm set" ;
  }

  chatter.publish( &str_msg );
  nh.spinOnce();

  delay(500);
}
